import React from 'react';


function Footertop(props){
    return(
        <div className="container-fluid">    
            <div className="container">
                <div className="site-footer-inner">
                    <div className="footer-inner">
                        <img src={props.Footerlogo} alt="myPic" className="Footerimg-sec" />
                            <div className="Footertop-menu">
                                <ul>
                                    <i className= {props.iconfacebook} ></i>
                                        <i className= {props.icontwitter} ></i>
                                            <i className= {props.instagramicon} ></i>
                
                                </ul>
                            </div>
                    </div>
                        <br />
                            <div className="Footer-sec">
                                <div className="footer-copyright">
                                    <h6>{props.Footer1}<span>{props.Footer2}</span> {props.Footer3}</h6>
                                </div>
                                    <div className="Footer-menu">
                                        <ul>
                                            <li><a href="default.asp">{props.Footerlist1}</a></li>
                                            <li><a href="news.asp">{props.Footerlist2}</a></li>
                                            <li><a href="contact.asp">{props.Footerlist3}</a></li>
                                            <li><a href="about.asp">{props.Footerlist4}</a></li>
                                        </ul>       
                                    </div>
                            </div>
                </div>               
            </div>
        </div> 
    );
};
export default Footertop;