//Header Folder in Header.jsx
import React from 'react';

function Header(props){
    return(
        <div className="container-fluid">
            <div className="container">
                <div className="header-sec">
                    <div className="imges">
                        <img src={props.img} alt=""/>
                    </div>
                    <div className="list-sec">
                        <ul>
                            <li> {props.list} </li>
                            <a href="ddd" className="button1">{props.link}</a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        
        
    );
};

export default Header;

