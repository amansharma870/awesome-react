import React from 'react';

import { useState } from 'react';



function Previewing(props){

    const[name, setname] = useState("");
const[fullName, setFullName] = useState();

const InputEvent = (event) => {
    //console.log(event.target.value);
    setname(event.target.value);
};

const onSubmit = () => {
    setFullName(name);
};
  
    return(
        <div className="container-fluid">
            <div className="container">
                <div className="cta-inner-sec">
                   
                    <div className="cta-slogan">
                        <h3>{props.Previewing1}</h3>
                    </div>
                    <div className="input-sec">  
                 <h5 className="email-sec">{fullName}</h5>      
                        <input type="email" id={props.Previewinginput} placeholder="Your best email" onChange={InputEvent} value={name} required />
                        <i onMouseEnter={onSubmit} className={props.Previewingicon} aria-hidden="true"></i>
                   </div>
                   
                </div>
            </div>
        </div>
       
     );
  };

export default Previewing;
