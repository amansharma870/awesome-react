
import React from 'react';
import ReactDOM  from 'react-dom';

import Header from './components/Header/Header';

 import Startups from './components/Slider-contect/Startups';
 import Picture from './components/Card-Content/Picture';
import Cardlist from './components/Content-Card/Cardlist';
import './index.css';
import Sdata from './components/Content-Card-data/Sdata';

import Workcards from './components/Work-Content-Card/Workcards';
import Data from './components/data-work/Data';


import Titleimg from './components/Card-Customer/Titleimg';
import Titlecard from './components/Data-Card-Customer/Titlecard';
import Previewing from './components/Previewing-content/Previewing';
import Footertop from './components/Footer/Footertop';
//import './MediaQueries.css';





ReactDOM.render(

    <div>
  
                <Header 
              
                img = "https://open.cruip.com/static/media/logo.2810a88b.svg"
                list="Documentation"
                link="Sing up"
                />
               
                <Startups 
                
                title="Landing template for"
                title2="startups"
                para="Our landing page template works on all devices, so you only have"
                parar="to set it up once, and get beautiful results forever."
                button2="Get started"
                button3="View on Github"
                viedo="https://open.cruip.com/static/media/video-placeholder.a622fc5d.jpg"
                /> 

              <Picture 
         
              picture= "Build up the whole picture"
              para1="Excepteur sint occaecat cupidatat non proident, sunt in culpa qui"
              para2="officia deserunt mollit anim id est laborum — semper quis lectus"
              para3="nulla at volutpat diam ut venenatis."
              /> 
           
              <div className="container-fluid">
                  <div className="container">
                    {Sdata.map((val) => {
                      return (
                        <Cardlist 
                            key={val.id}
                            imgessrc={val.imgessrc}
                            titles={val.titles}
                            titles1={val.titles1}
                            titles2={val.titles2}
                            titles3={val.titles3}
                            titles4={val.titles4}
                        />           
                          );
                        })}     
                          <hr className="new1"></hr>
                  </div>
                </div>
                <Picture          
                  picture= "Workflow that just works"
                  para1="Excepteur sint occaecat cupidatat non proident, sunt in culpa qui"
                  para2="officia deserunt mollit anim id est laborum — semper quis lectus"
                  para3="nulla at volutpat diam ut venenatis."
                />                     
                     <div className="container-fluid">
                          <div className="container">
                            {Data.map((val) => {
                              return (
                                <Workcards 
                                  key={val.id}
                                  text={val.text}
                                  date={val.date}
                                  titles4={val.titles4}
                                  titles5={val.titles5}
                                  titles6={val.titles6}
                                  titles7={val.titles7}
                                  titles8={val.titles8}
                                  img={val.img}
                                  />  
                                );
                                })}     
                                  <hr className="new1"></hr>
                          </div>
                        </div>
                           
              <Picture 
                  picture= "Customer testimonials"
                  para1="Excepteur sint occaecat cupidatat non proident, sunt in culpa qui"
                  para2="officia deserunt mollit anim id est laborum — semper quis lectus"
                  para3="nulla at volutpat diam ut venenatis."
              />

                <div className="container-fluid">
                    <div className="container">
                      <div className="cardlist3-sec">
                          {Titlecard.map((val) => {
                            return (
                              <Titleimg 
                                  key={val.id}
                                  Titlecardimg={val.Titlecardimg}
                                  Titlecardpara1={val.Titlecardpara1}
                                  Titlecardpara2={val.Titlecardpara2}
                                  Titlecardpara3={val.Titlecardpara3}
                                  Titlecardpara4={val.Titlecardpara4}
                                  Titlecardpara5={val.Titlecardpara5}
                                  Titlecardpara6={val.Titlecardpara6}
                                  Titlecardpara7={val.Titlecardpara7}
                                  Titlecardpara8={val.Titlecardpara8}
                                  test1={val.test1}
                                  test2={val.test2}
                                  test3={val.test3}
                              />  
                            );
                            })}     
                      </div>
                    </div>
                </div>
                       
                      
                        <Previewing 
                          
                         Previewing1= "For previewing layouts and visual?"
                         Previewinginput="email"
                         Previewingicon="fa fa-arrow-right"
                        
                        />
                        
          
                        <Footertop 
                            Footerlogo ="https://open.cruip.com/static/media/logo.2810a88b.svg"
                            iconfacebook="fa fa-facebook-f"
                            icontwitter="fa fa-twitter"
                            instagramicon="fa fa-instagram"
                            Footer1="Made by"
                            Footer2="Cruip."
                            Footer3="All right reserved"
                            Footerlist1="Contact"
                            Footerlist2="About us"
                            Footerlist3="FAQ's"
                            Footerlist4="Support"
                        />
                      
                   
               
    </div>,
document.getElementById('root')
);
